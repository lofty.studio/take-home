# Frequently Asked Questions

### Is this really a practical problem? Or just a purely mathematical riddle?
The problem is not a strict mathematical one with a single correct way to solve it. As long as Jack can reach the fridge within the time you provide in the result, it’s a correct one. But obviously, Jack’s peace of mind and wellbeing depends on the studio he chooses to rent. So the quicker the route you can find, the better!

### Can Jack's route be counted from any point on the bed’s outer boundary to any point on the refrigerator’s outer boundary?
Yes, sir!

### Can I consider Jack to be a point? Could he squeeze through a 10-centimeter hole?
Yep, he’s a pretty skinny guy. He can if he tries hard enough!

### Can Jack change direction while taking a single step?
He shouldn't. Jack's step is a straight line segment, max 80 cm long. The segment cannot intersect with any objects. You don’t want Jack to hit an obstacle even with his little toe while he’s rushing to the fridge at night, right?
