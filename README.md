# Lofty take-home coding challenge

Your best friend Jack knows you’re great at coding and solving difficult challenges, so he called you to ask for help. “Hey buddy,” he said, “I have this weird problem, you see. It’s a bit sensitive, but I’m sure fun for you to solve! Do you have a minute?”

“Sure, go ahead!”

“Right! Brace yourself, it’s not all that fun for me – but with your help, I’m sure I’ll handle it. You see, ever since the coronavirus epidemic broke out, every now and then I wake up in the middle of the night – panting, sweating and panicking. I’m afraid that I’ve run out of food!”

“Wow, that must be pretty nerve wracking.” You couldn’t think of much else to say to support poor Jack.

“My therapist,” he went on, “told me there is a good solution for this. And the advice comes in nice and timely! You remember I’m looking for a new studio to rent, right?”

“Oh, yeah. Sure thing, man. I’m on the lookout for you!”

“Thanks, buddy! So, here’s the thing. My problem will be solved if I can reach the fridge fast enough from my bed when I wake up at night. The therapist is confident it’ll help and I’m all full of hope too! But there are thousands of offers out there on the internet and no easy way to compare how long it takes to reach the fridge from the bed in each of them. Can you help me with that? Sounds like a fun coding challenge, doesn’t it?”

## Your task

Jack is browsing apartments for rent in an online property listing service. For each listing, he needs to know how fast he can walk from the bed to the fridge – specifically, how many steps he needs to take to reach the fridge after getting up from his bed, knowing that Jack’s step measures up to 80 cm.

The service that Jack uses provides handy data about each apartment in the following JSON format:

```js
{
    "apartment_dimensions": {
        "min_x": 0,
        "max_x": LENGTH,
        "min_y": 0,
        "max_y": LENGTH
    },
    "objects": [
        {
            "type": OBJECT_TYPE,
            "min_x": LENGTH,
            "max_x": LENGTH,
            "min_y": LENGTH,
            "max_y": LENGTH
        },
        ...
    ],
    ...
}
```

Each `LENGTH` value is expressed in centimeters, as a number rounded to the nearest multiple of 10 cm. Each `OBJECT_TYPE` is a string – and you can be sure Jack will only check apartments with exactly one fridge (`object.type == "FRIDGE"`) and exactly one bed (`object.type == "BED"`). Other objects in the apartment are things like walls, furniture and home equipment – obstacles that Jack needs to bypass on his way from the bed to the fridge.

Prepare a program that will return the minimum number of steps Jack needs to make to walk from the bed to the fridge, given data in the above format.

## Helpful tips and nice to haves

1. Jack has prepared a bunch of example data in the `fixtures/` folder to help you develop the program
1. Jack is a systems administrator. So the nicest way to run your program for him is to spin up a server with one or two simple commands, send JSON as input and get the number of steps as output. For example:
    ```bash
    ❯ curl --request POST \
          --data "$(cat fixtures/apartment-1.json)" \
          http://localhost:4422
    ❮ 5 steps from bed to fridge in that one, Jack
    ```
1. Jack isn’t nearly as good at coding as you are, but he likes learning from you and hacking away at side projects. You can be sure he’ll be tinkering with your code later on, trying to figure out how it works and how you came up with the solution. There’s a bunch of good practices you can use to make that easier for him:
    * make sure the code has helpful comments wherever it’s not self-documenting
    * make it easy to spin things up for development
    * if there’s tricky logic that Jack might miss when playing with the code, protect it (and “document” it) with unit tests
    * if you’re working with git, keep the history clean and commit messages descriptive.

Keep in mind that none of the tips above are required, so don’t worry if something you see there is new to you. During your work at Lofty you’ll need these skills sooner or later, but there’ll be time to learn them if you need to 👍

## By the way

If anything is unclear, hopefully the [FAQ](./FAQ.md) will help. Also, feel free to ping me anytime at [@architectcodes](https://messenger.com/t/architectcodes), [794652748](tel:0048794652748) or [tomek@lofty.studio](mailto:tomek@lofty.studio) with any other questions you may have.

Also, if you feel like this exercise is too time consuming for your situation, definitely let me know and we’ll figure out something lighter touch 🙂

Good luck! ✌️  
Tomek (@architectcodes)
